# echo-client.py
import socket
import _thread


CURSOR_UP_ONE = '\x1b[1A'
ERASE_LINE = '\x1b[2K'

def afficher(s):
    while True:
        data = s.recv(1024)
        user = s.recv(1024)
        data=data.decode()
        print("<"+user.decode()+"> "+data)
    
HOST = "127.0.0.1"  
PORT = 8081 


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    s.connect((HOST, PORT))
    
    user=input("Entrez un nom d'utilisateur: ")
    s.send(user.encode())

    _thread.start_new_thread(afficher,(s,))
    while True:
        msg=input()
        print("\033[0;32m"+CURSOR_UP_ONE + ERASE_LINE + "<you> " +msg + "\033[0;0m")
        s.send(msg.encode())
