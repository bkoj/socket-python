import socket
import _thread
import queue


class Server():
    def __init__(self,host,port,nb_clients):

        print("Creating server socket")
        self.s = socket.socket()

        self.HOST=host
        self.PORT=port
        self.clients=[]

        print(" Binding to ",self.HOST+":"+str(self.PORT))
        self.s.bind((self.HOST, self.PORT))

        print("Server listening..")
        self.s.listen(nb_clients)
        self.q=queue.Queue()

    def accept_connection(self):
    
        while True:
            new_client=self.s.accept()
            if new_client:
                new_client=Client(new_client)
                self.clients.append(new_client)
                _thread.start_new_thread(new_client.run,(self.q,))
                print(" client :",new_client.addr," connected ")
    
    def run(self):
            _thread.start_new_thread(self.accept_connection, ())
            while True:   
                queue_getter=self.q.get()
                if queue_getter[1]=="exit":
                    self.remove(queue_getter[0])
                else:
                    self.broadcast(queue_getter[1],queue_getter[0])

    def remove(self,client):
        if client in self.clients:
            client.clientsocket.close()
            print("Client ",client.addr ," disconnected")
            self.clients.remove(client)
            

    def broadcast(self,message, connection):
        for client in self.clients:
            if client.clientsocket != connection.clientsocket:
                try:
                    client.clientsocket.send(message)
                    client.clientsocket.send(connection.user.encode())
                except:
                    self.remove(connection)
            

class Client():
    def __init__(self,tupleC):
        self.clientsocket = tupleC[0]
        self.addr = tupleC[1]
        self.user=""

    def run(self,q):
        self.user=self.clientsocket.recv(1024).decode()

        while True:  
            msg = self.clientsocket.recv(1024)
            if msg:      
                print (self.addr, ' >> ', msg.decode())
                q.put([self,msg])
            else:
                q.put([self,"exit"])
                return False
            

serv=Server("192.168.1.48", 8081, 1)
serv.run()
